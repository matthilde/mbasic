# Poggers makefile
FILES = basic.c
FLAGS = -std=c99 # TODO: add -O1
OUT = basic

$(OUT): $(FILES)
	$(CC) $(FILES) $(FLAGS) -o $(OUT)
